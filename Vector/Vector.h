#ifndef VECTOR_H_
#define VECTOR_H_

namespace SkyBae {

template <typename Object>
class Vector {
private:
    int _size;
    int _capacity;
    Object* _objects;
public:
    static const int SPARE_CAPACITY = 16;

    explicit Vector(int size);
    Vector(const Vector &rhs);
    Vector(Vector &&rhs);
    Vector &operator=(Vector && rhs)
    Vector &operator=(const Vector & rhs);
    ~Vector();

    void resize(int newSize);
    void reserve(int newCapacity);

    Object &operator[](int index);
    const Object &operator[](int index) const;
    bool empty() const;
    int size() const;
    int capacity() const;

    void push_back(const Object &x);
    void push_back(Object &&x);
    void pop_back();
    const Object &back() const;

    typedef Object* iterator;
    typedef const Object* const_iterator;

    iterator begin();
    const_iterator begin() const;

    iterator end();
    const_iterator end() const;
};

}


#endif
