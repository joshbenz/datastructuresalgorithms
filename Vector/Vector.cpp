#include <algorithm>

namespace SkyBae {

explicit Vector::Vector(int size=0) : _size(size), _capacity(size + SPARE_CAPACITY) {
        _objects = new Object[_capacity];
}

Vector::Vector(const Vector &rhs) : _size(rhs._size),
                                    _capacity(rhs._capacity),
                                    _objects(nullptr)
{
    objects = new Object[_capacity];
    for(int i=0; i<_size; i++) {
        _objects[i] = rhs._objects[i];
    }
}

Vector::Vector(Vector &&rhs) : _size(rhs._size),
                                _capacity(rhs._capacity),
                                _objects
{

}
}
